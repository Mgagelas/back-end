<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group([
], function () { // custom admin routes
    Route::any('/failedLogin', 'Api\UserController@failedLogin');
    Route::post('/login', 'Api\UserController@login');
    Route::post('/register', 'Api\UserController@register');
    Route::group(['middleware' => 'auth:api'], function () {
    });
}); // this should be the absolute last line of this file






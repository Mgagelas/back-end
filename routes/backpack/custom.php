<?php

// --------------------------
// Custom Backpack Routes
// --------------------------
// This route file is loaded automatically by Backpack\Base.
// Routes you generate using Backpack\Generators will be placed here.

Route::group([
    'prefix'     => config('backpack.admin.route_prefix', 'admin'),
    'middleware' => ['web', config('backpack.admin.middleware_key', 'admin')],
    'namespace'  => 'App\Http\Controllers',
], function () { // custom admin routes
    Route::get('logout', 'Auth\LoginController@logout');
    Route::crud('vehicle', 'Admin\VehicleCrudController');
    Route::get('viewer','Admin\ViewerController@view');

    Route::get('new/vehicle','Admin\VehicleCrudController@newVehicleGet');
    Route::post('new/vehiclePost','Admin\VehicleCrudController@newVehiclePost');

    Route::get('add/{vehicle}/{id}','Admin\VehicleCrudController@addVehicleGet');
    Route::post('add/{vehicle}/{id}','Admin\VehicleCrudController@addVehiclePost');
    Route::crud('vehicleAssets', 'Admin\VehicleAssetsCrudController');
    Route::crud('vehicleCategory', 'Admin\VehicleCategoryCrudController');
    Route::get('vehicleCategory/add/{id}', 'Admin\VehicleCategoryCrudController@addGet');
    Route::post('vehicleCategory/add/{id}', 'Admin\VehicleCategoryCrudController@addPost');
    Route::get('vehicleCategory/remove/{id}', 'Admin\VehicleCategoryCrudController@removeGet');
    Route::post('vehicleCategory/remove/{id}', 'Admin\VehicleCategoryCrudController@removePost');
}); // this should be the absolute last line of this file

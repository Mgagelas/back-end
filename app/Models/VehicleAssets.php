<?php

namespace App\Models;

use Backpack\CRUD\app\Models\Traits\CrudTrait;
use Illuminate\Database\Eloquent\Model;

class VehicleAssets extends Model
{
    use CrudTrait;
    protected $fillable = [
        'location',
    ];
    //
    public function setLocationAttribute($value)
    {
        $this->attributes['logo'] = $value;
        $attribute_name = "logo";
        $disk = "public";
        $destination_path = "/storage/logo/".$this->folderName;
        $this->uploadFileToDisk($value, $attribute_name, $disk, $destination_path);
    }
}

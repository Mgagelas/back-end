<?php

namespace App\Models;

use Backpack\CRUD\app\Models\Traits\CrudTrait;
use Illuminate\Database\Eloquent\Model;

class VehicleCategory extends Model
{
    use CrudTrait;
    public $timestamps = false;
    protected $fillable = [
        'name', 'description',
    ];
    public function vehicles ()
    {
        return $this->belongsToMany('App\Models\Vehicle','App\Models\VehicleVehicleCategory','vehicle_id','categories_id','id','id');
    }
    public function addAircraft($crud = false)
    {
        $text = 'vehicleCategory/add';
        return '<a class="btn btn-sm btn-link" href="'.$text.'/'.$this->id.'" data-toggle="tooltip"><i class="nav-icon la la-plus"></i>Add aircraft\'s</a>';
    }
    public function removeAircraft($crud = false)
    {
        $text = 'vehicleCategory/remove';
        return '<a class="btn btn-sm btn-link" href="'.$text.'/'.$this->id.'" data-toggle="tooltip"><i class="la la-minus"></i>Remove aircraft\'s</a>';
//        $text = 'creator/?token=' . $data ['token'] . '&task=' . $data['task'] . '&category=' . $data['category'] . '&productId=' . $data['productId'] . '&sectionId=' . $data['sectionId'] . '&companyId=' . $data['companyId'];
//        return '<a class="btn btn-sm btn-link" target="_blank" href="'.$text.'" data-toggle="tooltip"><i class="fa fa-search"></i>View it</a>';
//
    }
}

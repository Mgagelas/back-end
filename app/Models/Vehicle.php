<?php

namespace App\Models;

use Backpack\CRUD\app\Models\Traits\CrudTrait;
use Illuminate\Database\Eloquent\Model;

class Vehicle extends Model
{
    use CrudTrait;
    public function category ()
    {
        return $this->belongsToMany('App\Models\VehicleCategory','App\Models\VehicleVehicleCategory','vehicle_id','categories_id','id','id');
    }
}

<?php

namespace App\Http\Controllers\Admin;

use App\Models\Vehicle;
use App\Models\VehicleAssets;
use App\Models\VehicleSummary;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use App\Http\Requests\PoseRequest;
use Carbon\Carbon;
use Illuminate\Http\Request;
use MongoDB\Driver\Session;

class VehicleAssetsCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;

    public function setup()
    {
        $this->crud->setModel("App\Models\VehicleAssets");
        $this->crud->setRoute("admin/vehicleAssets");
        $this->crud->setEntityNameStrings('vehicleAsset', 'vehiclesAssets');
    }

    protected function setupListOperation()
    {
        $this->crud->addColumn([ // image
            'label' => "location",
            'name' => "location",
            'type' => 'image',
        ]);
    }

    protected function setupCreateOperation()
    {
        $this->crud->addField([ // image
            'label' => "location",
            'name' => "location",
            'type' => 'image',
        ]);

    }

    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }
}

<?php

namespace App\Http\Controllers\Admin;

use App\Models\Vehicle;
use App\Models\VehicleAssets;
use App\Models\VehicleGroup;
use App\Models\VehicleSummary;
use App\Models\VehicleVehicleCategory;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use App\Http\Requests\PoseRequest;
use Carbon\Carbon;
use Illuminate\Http\Request;
use MongoDB\Driver\Session;

class VehicleCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;

    public function setup()
    {
        $this->crud->setModel("App\Models\Vehicle");
        $this->crud->setRoute("admin/vehicle");
        $this->crud->setEntityNameStrings('vehicle', 'vehicles');
    }

    protected function setupListOperation()
    {
        $this->crud->setFromDb();
    }

    protected function setupCreateOperation()
    {
        $this->crud->setFromDb();
    }

    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }

    public function newVehicleGet()
    {
        $vehicleGroups = ['1' => 'Airplane', '2' => 'Helicopter', '3' => 'Cargo', '4' => 'Drone'];
        return view('model_upload')->with('vehicleGroups', $vehicleGroups);
    }

    public function newVehiclePost(Request $request)
    {
        $vehicleGroup = VehicleGroup::where('name',$request->vehicleGroupS)->get();
        dd($vehicleGroup);
        $timestamp = Carbon::now();
        $vehicleId = Vehicle::insertGetId(
            [
                'name' => $request->modelNameT,
                'vehicle_group_id' => $request->vehicleGroupS,
                'accessible' => 0,
                "created_at" => $timestamp,
                "updated_at" => $timestamp
            ]);
        VehicleVehicleCategory::insertGetId(
            [
                'vehicle_id' => $vehicleId,
                'categories_id' => $request->vehicleGroupS,
            ]);
        foreach ($request->file('modelF') as $modelUpload) {
            $fullFileName = explode('.', $modelUpload->getClientOriginalName());
            $fileName = $fullFileName[0];
            $fileExtension = last($fullFileName);
            //android file upload name hash
            $modelPath = $modelUpload->storeAs("storage/models/{$vehicleId}/obj/{$fileName}.{$fileExtension}", "public");
            $modelPath = $modelUpload->storeAs('storage/models/' . $vehicleId . '/obj/', $fileName . '.' . $fileExtension, "public");
        }

        return redirect('admin/add/' . strtolower($request->vehicleGroupS) . '/' . $vehicleId)->with('vehicleGroup', $request->vehicleGroupS)->with('modelNameT', $request->modelNameT);
    }

    public function addVehicleGet($vehicle, $id)
    {
        if (!\Illuminate\Support\Facades\Session::get('vehicleGroup') == null) {
            $formPost = ['vehicle' => $vehicle, 'id' => $id];
            return view('model_additional_upload')->with('vehicleGroup', \Illuminate\Support\Facades\Session::get('vehicleGroup'))->with('modelNameT', \Illuminate\Support\Facades\Session::get('modelNameT'))->with('formPost', $formPost);
        } else {
            return redirect('admin/new/vehicle')->withErrors(["your_custom_error" => "Something went wrong, nothing was saved"]);
        }
    }

    public function addVehiclePost(Request $request, $vehicle, $id)
    {
        $timestamp = Carbon::now();
        VehicleSummary::insert(
            [
                'vehicle_id' => $id,
                'summary_data' => $request->summary,
                "created_at" => $timestamp,
                "updated_at" => $timestamp
            ]);
        //pdfs
        foreach ($request->allFiles()['pdf'] as $pdf) {
            $fullFileName = explode('.', $pdf->getClientOriginalName());
            $fileName = $fullFileName[0];
            $fileExtension = last($fullFileName);
            //android file upload name hash
            $pdfPath = $pdf->storeAs('storage/models/' . $id . '/pdf/', $fileName . $fileExtension, "public");
            $assets = VehicleAssets::insert(
                [
                    'assetable_id' => $id,
                    'assetable_type' => 'App\Models\Vehicle',
                    'type' => $vehicle,
                    'location' => $pdfPath,
                    "created_at" => $timestamp,
                    "updated_at" => $timestamp
                ]
            );
        }
        return redirect('admin/new/vehicle');
    }
}

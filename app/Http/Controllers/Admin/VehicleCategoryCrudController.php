<?php

namespace App\Http\Controllers\Admin;

use App\Models\Vehicle;
use App\Models\VehicleAssets;
use App\Models\VehicleCategory;
use App\Models\VehicleSummary;
use App\Models\VehicleVehicleCategory;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use App\Http\Requests\PoseRequest;
use Carbon\Carbon;
use Illuminate\Http\Request;
use MongoDB\Driver\Session;

class VehicleCategoryCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;

    public function setup()
    {
        $this->crud->setModel("App\Models\VehicleCategory");
        $this->crud->setRoute("admin/vehicleCategory");
        $this->crud->setEntityNameStrings('Vehicle Category', 'Vehicle Categories');
        $this->crud->denyAccess('show');
        $this->crud->addButtonFromModelFunction('line', 'removeAircraft', 'removeAircraft', 'beginning');
        $this->crud->addButtonFromModelFunction('line', 'addAircraft', 'addAircraft', 'beginning');
    }
    protected function setupListOperation()
    {
        $this->crud->setFromDb();
    }

    protected function setupCreateOperation()
    {
        $this->crud->setFromDb();
    }

    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }
    protected function addGet($categoryid)
    {
        $action = 'Add';
        $post = 'Admin\VehicleCategoryCrudController@addPost';
        $category = VehicleCategory::where('id','=', $categoryid)->first();
        $airplanesData = Vehicle::whereDoesntHave('category')->get();
        $formPost = $categoryid;
        return view('addRemoveCategory')->with('action', $action)->with('post',$post)->with('formPost',$formPost)->with('category', $category)->with('airplanesData',$airplanesData);
    }
    protected function addPost(Request $request, $categoryid)
    {
        foreach($request->airplanes as $airplane)
        {
            $newAircraftInsert ['vehicle_id'] = $airplane;
            $newAircraftInsert ['categories_id'] = $categoryid;
            $fullAircaftInsert[]= $newAircraftInsert;
        }
        VehicleVehicleCategory::insert($fullAircaftInsert);
    }
    protected function removeGet($categoryid)
    {
        $action = 'Remove';
        $post = 'Admin\VehicleCategoryCrudController@removePost';
        $category = VehicleCategory::where('id','=', $categoryid)->first();
        dd($category);
        $airplanesData = Vehicle::with('category')->where('id','=', $categoryid)->get();
        $formPost = $categoryid;
        return view('addRemoveCategory')->with('action', $action)->with('post',$post)->with('formPost',$formPost)->with('category', $category)->with('airplanesData',$airplanesData);
    }
    protected function removePost(Request $request, $categoryid)
    {
        dd($request);
    }
}

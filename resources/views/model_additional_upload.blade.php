@extends(backpack_view('blank'))
<link rel="stylesheet" type="text/css" href="{{ url('/css/styles.css') }}"/>
@section('content')

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
    <script src="{{asset('js/extendForm.js')}}"></script>
    <h1>Upload all vehicle data</h1><br><br>
    <div class="vehicleUpload">
        {!! Form::open(['class'=>'uploadForm','id'=>'uploadForm','action' => ['Admin\VehicleCrudController@addVehiclePost',$formPost['vehicle'], $formPost['id']], 'method' => 'POST', 'enctype' => 'multipart/form-data']) !!}
        <div class="form-group1">
            {{Form::label('modelNameL', 'Model Name:')}}
            {{ Form::label('modelNameT', $modelNameT ?? 'unknown') }}
            <br>
            {{Form::label('vehicleGroupL', 'Model Group:')}}
            {{ Form::label('vehicleGroupS', $vehicleGroup ?? 'unknown') }}
            <br>
            <br>
            {{Form::label('summary', 'Summary:')}}
            <br>
            {{Form::textarea('summary', '', ['id' => 'summaryField', 'class' => 'summaryField', 'placeholder' => 'Summary', 'rows'=>'8', 'cols'=>'50'])}}
            <br>
            {{Form::label('pdf[]', 'Pdfs:')}}
            <br>
            <input type="file" name="pdf[]" id="fileToUpload" accept="application/pdf" multiple>
            <br>
            {{Form::label('pictures', 'Pictures:')}}
            <br>
            {{Form::file('pictures', ['accept'=>'image/*', 'multiple'])}}
            <br>
            {{Form::label('videos', 'Videos:')}}
            <br>
            {{Form::file('videos', ['accept'=>'video/*', 'multiple'])}}
            <br>
            {{Form::label('3in1picture', '3 in 1 picture:')}}
            <br>
            {{Form::file('pictures', ['accept'=>'image/*', 'multiple'])}}
            <br>
            {{Form::label('3in1picture', '3 in 1 picture top description:')}}
            <br>
            {{Form::text('3in1picture', '', ['id' => '3in1picture1', 'class' => 'picture3in1', 'placeholder' => 'Top', 'rows'=>'1'])}}
            <br>
            <br>
            {{Form::label('3in1picture', '3 in 1 picture middle description:')}}
            <br>
            {{Form::text('3in1picture', '', ['id' => '3in1picture2', 'class' => 'picture3in1', 'placeholder' => 'Middle','rows'=>'1'])}}
            <br>
            <br>
            {{Form::label('3in1picture', '3 in 1 picture bottom description:')}}
            <br>
            {{Form::text('3in1picture', '', ['id' => '3in1picture3', 'class' => 'picture3in1', 'placeholder' => 'Bottom','rows'=>'1'])}}
            <br>
        </div>
    </div>
    <br>
    {{Form::submit('Submit', ['class'=>'btn btn-primary', ''])}}
    {!! Form::close() !!}
    <button type="button" id="Confirm" name="Confirm">Click Me!</button>
@endsection

<link rel="stylesheet" type="text/css" href="{{ url('/css/styles.css') }}"/>
@extends(backpack_view('blank'))
@section('content')
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
    <script src="{{asset('js/extendForm.js')}}"></script>
<h1> {{$action}} aircraft's</h1><br><br>
    @if($errors->any())
        <div class="errorMessage">
            {!! implode('', $errors->all('<div>:message</div>')) !!}
        </div>
    @endif
    <p class ="headerParagraph"> Current category: {{$category->name}} </p>
    <p class ="headerParagraph"> Current category description: {{$category->description}} </p>
    <form id="addOrRemoveFromCategory" class="addOrRemoveFromCategory" method="post" action="{{action($post, $formPost)}}" enctype="multipart/form-data">
        @csrf
    <select type="airplanes" name="airplanes[]" class="airplanes" accept="application/pdf" multiple>
        @foreach($airplanesData as $airplaneData){
        <option value="<?php echo strtolower($airplaneData->id); ?>"><?php echo $airplaneData->name; ?></option>
        @endforeach
    </select>
        <input type="submit" value="Submit">
    </form>
@endsection



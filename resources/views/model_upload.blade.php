<link rel="stylesheet" type="text/css" href="{{ url('/css/styles.css') }}"/>
@extends(backpack_view('blank'))
@section('content')
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
    <script src="{{asset('js/extendForm.js')}}"></script>
<h1>Upload new vehicle</h1><br><br>
    @if($errors->any())
        <div class="errorMessage">
            {!! implode('', $errors->all('<div>:message</div>')) !!}
        </div>
    @endif
    <div class="vehicleUpload">
    {!! Form::open(['class'=>'uploadForm','id'=>'uploadForm','action' => 'Admin\VehicleCrudController@newVehiclePost', 'method' => 'POST', 'enctype' => 'multipart/form-data']) !!}
    <div class="form-group1">
        {{Form::label('modelNameL', 'Model Name:')}}
        {{Form::text('modelNameT', '', ['id' => 'article-ckeditor', 'class' => 'form-control', 'placeholder' => 'Model Name'])}}
        {{Form::label('vehicleGroupL', 'Model Group:')}}
        <br>
        <select type="vehicleGroupS" name="vehicleGroupS" class="vehicleGroupS" accept="application/pdf">
            @foreach($vehicleGroups as $key => $vehicleGroup){
            <option value="<?php echo strtolower($key); ?>"><?php echo $vehicleGroup; ?></option>
            @endforeach
        </select>

    </div>
    <div class="form-group2" id="form-group3">
        {{Form::label('model', 'Model')}}
        <br>
        {{Form::file('modelF[]', [ 'multiple'])}}
    </div>
    </div>
    <br>
    {{Form::submit('Submit', ['class'=>'btn btn-primary', ''])}}
    {!! Form::close() !!}

@endsection



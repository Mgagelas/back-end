<!-- This file is used to store sidebar items, starting with Backpack\Base 0.9.0 -->
<link rel="stylesheet" href="path/to/font-awesome/css/font-awesome.min.css">
<li class="nav-item"><a class="nav-link" href="{{ backpack_url('dashboard') }}"><i class="fa fa-dashboard nav-icon"></i> {{ trans('backpack::base.dashboard') }}</a></li>
<a class="nav-link" href="{{ backpack_url('vehicle') }}"><i class="nav-icon fa fa-cog"></i> <span>Vehicles</span></a>
<a class="nav-link" href="{{ backpack_url('new/vehicle') }}"><i class="nav-icon fa fa-cog"></i> <span>Add New Vehicle</span></a>
<a class="nav-link" href="{{ backpack_url('vehicleCategory') }}"><i class="nav-icon la la-cog"></i> <span>Categories</span></a>
<li class="nav-item nav-dropdown">
    <a class="nav-link nav-dropdown-toggle" href="#"><i class="nav-icon la la-users"></i> Authentication</a>
    <ul class="nav-dropdown-items">
        <li class="nav-item"><a class="nav-link" href="{{ backpack_url('user') }}"><i class="nav-icon la la-user"></i> <span>Users</span></a></li>
        <li class="nav-item"><a class="nav-link" href="{{ backpack_url('role') }}"><i class="nav-icon la la-id-badge"></i> <span>Roles</span></a></li>
        <li class="nav-item"><a class="nav-link" href="{{ backpack_url('permission') }}"><i class="nav-icon la la-key"></i> <span>Permissions</span></a></li>
    </ul>
</li>

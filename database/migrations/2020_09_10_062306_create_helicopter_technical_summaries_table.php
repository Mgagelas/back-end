<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateHelicopterTechnicalSummariesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('helicopter_technical_summaries', function (Blueprint $table) {
            $table->id();
            $table->string('Title');
            $table->string('Fuselage');
            $table->string('Nose');
            $table->string('Cockpit');
            $table->string('Tail boom');
            $table->string('Undercarriage');
            $table->string('Tail rotor');
            $table->string('Engines');
            $table->string('Tail pylon');
            $table->string('Stabiliser');
            $table->string('Stabiliser location');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('helicopter_technical_summaries');
    }
}

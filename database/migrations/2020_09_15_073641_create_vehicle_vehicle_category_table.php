<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVehicleVehicleCategoryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vehicle_vehicle_categories', function (Blueprint $table) {
            $table->integer('categories_id')->unsigned();
            $table->integer('vehicle_id')->unsigned();
            $table->primary(['categories_id','vehicle_id']);
            $table->foreign('categories_id')->references('id')->on('vehicle_categories')->onDelete('cascade');
            $table->foreign('vehicle_id')->references('id')->on('vehicles')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vehicle_vehicle_categories');
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAirplaneTechnicalSummariesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('airplane_technical_summaries', function (Blueprint $table) {
            $table->id();
            $table->string('Title');
            $table->string('Fuselage');
            $table->string('Nose');
            $table->string('Air intake');
            $table->string('Intake shape');
            $table->string('Wing position');
            $table->string('Wing shape');
            $table->string('Fin shape');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('airplane_technical_summaries');
    }
}

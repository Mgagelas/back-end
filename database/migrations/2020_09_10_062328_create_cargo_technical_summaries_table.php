<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCargoTechnicalSummariesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cargo_technical_summaries', function (Blueprint $table) {
            $table->id();
            $table->string('Title');
            $table->string('Fuselage');
            $table->string('Nose');
            $table->string('Engine type');
            $table->string('Engines location');
            $table->string('Sponsons');
            $table->string('Wing shape');
            $table->string('Fin shape');
            $table->string('Tailplane shape');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cargo_technical_summaries');
    }
}

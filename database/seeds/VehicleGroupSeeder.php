<?php

use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class VehicleGroupSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if (DB::table('vehicle_groups')->where('id', '1')->count() == 0) {
            DB::table('vehicle_groups')->insert([
                'name'     => 'Airplane',
                'description'    => 'Airplanes description',
            ]);
            DB::table('vehicle_groups')->insert([
                'name'     => 'Helicopter',
                'description'    => 'Helicopter description',
            ]);
            DB::table('vehicle_groups')->insert([
                'name'     => 'Cargo',
                'description'    => 'Cargo description',
            ]);
            DB::table('vehicle_groups')->insert([
                'name'     => 'Drone',
                'description'    => 'Drone description',
            ]);
        }
        factory(User::class, 0)->create();
    }
}

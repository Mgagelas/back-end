<?php

use Illuminate\Database\Seeder;

class VehicleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if (DB::table('vehicles')->where('name', 'AirplaneTest1')->count() == 0) {
            DB::table('vehicles')->insert([
                'name' => 'AirplaneTest1',
                'accessible' => 0,
                'vehicle_group_id' => 1
            ]);
        }
        if (DB::table('vehicles')->where('name', 'AirplaneTest2')->count() == 0) {
            DB::table('vehicles')->insert([
                'name' => 'AirplaneTest2',
                'accessible' => 0,
                'vehicle_group_id' => 1
            ]);
        }
        if (DB::table('vehicles')->where('name', 'AirplaneTest3')->count() == 0) {
            DB::table('vehicles')->insert([
                'name' => 'AirplaneTest3',
                'accessible' => 0,
                'vehicle_group_id' => 1
            ]);
        }
        if (DB::table('vehicles')->where('name', 'HelicopterTest1')->count() == 0) {
            DB::table('vehicles')->insert([
                'name' => 'HelicopterTest1',
                'accessible' => 0,
                'vehicle_group_id' => 1
            ]);
        }
    }
}

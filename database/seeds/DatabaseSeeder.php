<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(Database\Seeds\UsersTableSeeder::class);
        $this->call(PermissionManagerTablesSeeder::class);
        $this->call(VehicleGroupSeeder::class);
        $this->call(VehicleSeeder::class);
        $this->call(VehicleCategorySeederTableSeeder::class);
        $this->call(VehicleCategoryVehicleSeederTableSeeder::class);
    }
}

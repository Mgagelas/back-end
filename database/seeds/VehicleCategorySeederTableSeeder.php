<?php

use Illuminate\Database\Seeder;

// composer require laracasts/testdummy
use Laracasts\TestDummy\Factory as TestDummy;

class VehicleCategorySeederTableSeeder extends Seeder
{
    public function run()
    {
        if (DB::table('vehicle_categories')->where('name', 'Airplane')->count() == 0) {
            DB::table('vehicle_categories')->insert([
                'name' => 'Airplane',
                'description' => 'Airplane category',
            ]);
        }
        if (DB::table('vehicle_categories')->where('name', 'Helicopter')->count() == 0) {
            DB::table('vehicle_categories')->insert([
                'name' => 'Helicopter',
                'description' => 'Helicopter category',
            ]);
        }
        if (DB::table('vehicle_categories')->where('name', 'Cargo')->count() == 0) {
            DB::table('vehicle_categories')->insert([
                'name' => 'Cargo',
                'description' => 'Cargo category',
            ]);
        }
        if (DB::table('vehicle_categories')->where('name', 'Drone')->count() == 0) {
            DB::table('vehicle_categories')->insert([
                'name' => 'Drone',
                'description' => 'Drone category',
            ]);
        }
        // factory(\App\Models\SectionUser::class, 1)->create();
    }
}

<?php

use Illuminate\Database\Seeder;

// composer require laracasts/testdummy
use Laracasts\TestDummy\Factory as TestDummy;

class VehicleCategoryVehicleSeederTableSeeder extends Seeder
{
    public function run()
    {
        if (DB::table('vehicle_vehicle_categories')->where('categories_id', '1')->count() == 0) {
            DB::table('vehicle_vehicle_categories')->insert([
                'categories_id' => 1,
                'vehicle_id' => 1,
            ]);
        }
        // factory(\App\Models\SectionUser::class, 1)->create();
    }
}
